

/**
 * class RoutingPacket 
 * Purpose: 
 * Author: Durjoy Barua 
 * Student ID: 040919612
 * data members: 
 *   
 * methods: 
 * 	processFoundPacket - If the ip Packet found in the RoutingTable this method sends out the message
 * 	processNotFoundPacket - If the ip Packet not found in the RoutingTable this method sends the message about adding the entry to the routing table
 */
public class RoutingPacket extends Packet {


	public boolean processFoundPacket(String ipPacket) {
		//This method does the action needed if the IP Address of the packet is found in routing table
		//and Display the message of confirmation
		System.out.println("Entry is already in the routing table "+getDestNetwork());
		return true;
	}

	public boolean processNotFoundPacket(String ipPacket) {
		//This method runs if the IPAddress of the packet is not found
		System.out.println("Adding Entry to the routing table "+getDestNetwork());
		return false;
	}
}

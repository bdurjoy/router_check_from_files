

/**
 * class RoutingTableEntry
 * Purpose: 
 * Author: Durjoy Barua 
 * Student ID: 040919612
 * data members: 
 *   routingTableEntry - Arraylist
 *   destination - 
 *   portCode
 *   ipAddress
 * methods: 
 * 	RoutingTableEntry - Default constructor
 * 	addEntry -
 * 	toString - 
 * 	searchForPort - 
 * 	searchRoute - 
 */


public class RoutingTableEntry {
	private IPAddress destination = new IPAddress();
	private String portCode = new String();
	private IPAddress ipAddress = new IPAddress();

	public RoutingTableEntry() {
		//Default Constructor
//		destination = new IPAddress();
//		portCode = new String();
	}

	public void addEntry(IPAddress dest, String portCode) {
		//To update the information in the current object from the parameters passsed into the method
//		for(int i = 0; i < 4; i++) {
//			this.destination.address[i] = destination.address[i];
//		}
		destination.initialize(dest);
		this.portCode = portCode;
	}

	public String toString() {
		//to display this entry to a String

		String tableDisplay = new String();
		
		tableDisplay += destination.toString()+" Port: "+portCode;
		return tableDisplay;

		//		String tableDisplay = new String();
		//		
		//		for(int i = 0; i < 3; i++) {
		//			tableDisplay += destination.address[i]+".";
		//		}
		//		
		//		tableDisplay += destination.address[3];
		//		tableDisplay += "\\"+portCode;	
	}

	public String searchForPort(IPAddress destination) {
		//This method compares the destination IP Address in parameter to this entry to see if they match,
		//and return back the portCode 
		if(destination.isEqual(destination)) {
			return portCode;
		}else {
			return null;
		}
	}
	
	//This method I tried to use it for search route but I couldn't accomplish
//	public int compareAddress(RoutingTableEntry routingTableEntry) {
//		return this.ipAddress.compareTo(routingTableEntry);
//	}
	
	
	
}

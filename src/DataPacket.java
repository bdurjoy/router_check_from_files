/**
 * class DataPacket 
 * Purpose: 
 * Author: Durjoy Barua 
 * Student ID: 040919612
 * data members: 
 *   
 * methods: 
 * 	processFoundPacket - This method is for sending the packet information out to the file
 * 	processNotFoundPacket - This method send the information if the packet is not found
 */



public class DataPacket extends Packet {


	public boolean processFoundPacket(String ipPacket) {
		//This method does the action needed if destination IP address of the packet is found in the routing
		//table - which display a message which port the packet is exiting
		System.out.println("Sending Packet out "+ipPacket+" "+getDestNetwork());
		return true;
	}

	public boolean processNotFoundPacket(String ipPacket) {
		//If destination IP address of the packet is not found in the routing table,
		//this method shows the message of dropping the packet
		System.out.println("Dropping Packet.... "+getDestNetwork());
		return true;

	}
}

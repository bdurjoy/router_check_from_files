/**
 * class Packet 
 * Purpose: 
 * Author: Durjoy Barua 
 * Student ID: 040919612
 * data members: 
 *   sourceAddress - Object of IPAddress that takes source address information
 *   destinationAddress - Object of IPAddress that takes destination address information
 *   packetData - Create String object for the packet data information
 * methods:
 * 	readPacket - checks the value of Scanner if it's read the proper data for the program
 * 	getDestNetwork - returns the network IPAddress of the destination address
 * 	processFoundPacket - This method does the action needed if destination IP address of the packet is found in the routing table
 * 	processNotFoundPacket - This method does the action needed if destination IP address of the packet is not found in the routing table
 * 	getPacketData - Return the value of the packetData
 */
import java.util.Scanner;



public class Packet{
	private IPAddress sourceAddress; //Object of type IPAddress for the destination address
	private IPAddress destinationAddress; //Object of type IPAddress for the source address
	private String packetData; //String object for packetData

	public Packet() {
		//Default Constructor
		sourceAddress = new IPAddress();
		destinationAddress = new IPAddress();
		packetData = new String("");
	}

	public boolean readPacket(Scanner readPacket) {
		//This method checks the value of Scanner if it's read the proper data for the program
		boolean read = false;
		if(sourceAddress.readAddress(readPacket) && destinationAddress.readAddress(readPacket)) {
			read = true;
		}
		if(read) {
			packetData = readPacket.next();//This line reads the packetData
		}
		return read;
	}

	public IPAddress getDestNetwork() {
		//This method returns the network IPAddress of the destination address

		return destinationAddress.getNetwork();	
	}

	public boolean processFoundPacket(String destIp) { //This method does the action needed if destination IP address of the packet is found in the routing table
		return true;
	}

	public boolean processNotFoundPacket(String destIp) { //This method does the action needed if the destination IP address of the packet is not found in the routing table
		return true;
	}
	public String getPacketData() {
		//This method returns the packetData String

		return packetData;
	}
}

/**
 * class Router
 * Purpose: 
 * Author: Durjoy Barua 
 * Student ID: 040919612
 * data members: 
 *   routingTable - Assigns a RoutingTableEntry array of the object
 *   
 * methods: 
 * 	Router() - Default constructor that defines the arraysize and asks the user for the size
 * 	processPacket -
 * 	displayTable -
 */


import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;



public class Router {
	private ArrayList<RoutingTableEntry> routingTable; //RoutingTableEntry objects to dynamically allocate array
	//private int maxEntries; //max number of entries could allocate in the array
	//private int numEntries; //number of entries currently allocated to the routing table

	public Router() {
		//This default constructor asks the user to insert number entries in the table and initiate the array
		Scanner input = new Scanner(System.in);
		int size = 0;
		boolean end = false;
		while(end == false) {
			System.out.println("Enter maximum number of entries for array: ");
			try {
				size = input.nextInt();
				if(size <= 0)
					throw new InputMismatchException();

				end = true;
				
			}catch(InputMismatchException iM) {
				System.out.println("Entry not valid!");
				input.next();
			}
		}
		routingTable = new ArrayList<RoutingTableEntry>(size);
	}

	public void processPacket(Packet packet) {

		//This method will have a parameter of a packet it will first,
		//try to find the destination address in the existing
		//table. it will then call the appropriate method - 
		//processFoundPacket or processNotFoundPacket in the 
		//appropriate packet class
		String pkt = null;
		boolean pkFound = false;
		int i = 0;
		while(i < routingTable.size()) {
			pkt = routingTable.get(i).searchForPort(packet.getDestNetwork());

			if(pkt != null) {
				pkFound = true;
				break;
			}
			i++;
		}
		boolean addToTable = false;
		if(pkFound) {
			addToTable = packet.processFoundPacket(pkt);
		}else {
			addToTable = packet.processNotFoundPacket(pkt);
		}
		
		
		if(addToTable) {
				RoutingTableEntry routingEntry = new RoutingTableEntry();
				routingEntry.addEntry(packet.getDestNetwork(), packet.getPacketData());
				routingTable.add(routingEntry);
				
			}
		

		

	}

	public void displayTable() {
		//This method will display the current Routing table
		System.out.println("Routing Table.....");

		for(RoutingTableEntry routeEntry: routingTable) {
			System.out.println(routeEntry);
		}
	}
	
	public int searchRoute(RoutingTableEntry req) {
		int low = 0;
		int high = routingTable.size();
		
		while(low < high) {
			int middle = (low+high)/2;
			int compare; //= req.compareAddress(routingTable.get(middle));
			if(middle<0) {
				low = middle +1;
			}else if(middle > 0) {
				high = middle - 1;
			}else {
				return middle;
			}
		}
		return -(low+1);
		
	}


	



}

/**
 * class DataMain
 * Purpose: 
 * Author: Durjoy Barua 
 * Student ID: 040919612
 * data members: 
 *   input - Scanning the information from the user
 *   router - Later it creates a object of router
 *   finish - boolean for checking the end of the program
 *   pack - String variable for packet handling
 *   packet - For the purpose of creating a generic packet
 *   checkChar - This char variable for checking files input of each line
 * methods: 
 * 	
 */


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // Initialization of Scanner
		Router router; //Declaration of a router object
		boolean finish = false; //boolean value keep the program running
		String pack;//String for packet handling
		Packet packet = null; //creating generic packet
		char checkChar;

		do{
			router = new Router();

			System.out.println("Enter the file name: "); //asks the user to input filename
			String fileName = input.next();
			


			try {
				File fileRead = new File(fileName); //fileread that takes input of file name
				Scanner reader = new Scanner(fileRead);//String fileData; 
				while(reader.hasNext()) {
					checkChar = reader.next().charAt(0);
					if(checkChar == 'p') {
						//update the routing table
						packet = new RoutingPacket();
					}else if(checkChar == 'd') {
						//packet should be routed with data
						packet = new DataPacket();
					}

					if(packet != null && packet.readPacket(reader)) {
						router.processPacket(packet);
					}


				}
				
				finish = true;//sets up finish to true for breaking the program after while loop
				router.displayTable();//display final router table
				

			}catch(FileNotFoundException fN) {
				System.out.println("File not found!");
			} catch (IOException iE) {
				System.out.println("");
			}


		}while(finish == false);

	}
}
